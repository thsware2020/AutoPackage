---
title: Linux下常用的环境安装
tags: Linux
categories: 折腾
description: 该文章是日常折腾Linux过程中遇到的一些问题。主要为环境的安装。大部分为Centos
abbrlink: 46610
date: 2021-04-16 02:20:53
---

# NodeJS安装

> 由于使用yum仓库安装的nodejs为低版本，VueCli无法正常使用

~~~shell
#安装最新的Nodejs存储库
curl -sL https://rpm.nodesource.com/setup_15.x | sudo bash -
#建立元数据缓存
sudo yum clean all && sudo yum makecache fast
#安装Gcc
sudo yum install -y gcc-c++ make
#安装NodeJS
sudo yum install -y nodejs
#查看版本
node -v
~~~

